---
title: Ingreso al sistema.
---



# Primer Ingreso

Ingreso por primera vez a la página: [https://mirincon.ar/](https://mirincon.ar/)



Deberás registrarte ingresando a “**Soy Nuevo**”, al hacer clic en **REGISTRATE!



![image-20230817124925458](./image-20230817124925458.png)



Deberás poner tu número de documento “sin puntos” y hacer clic en “VALIDAR DOCUMENTO”



![image-20230817124944579](./image-20230817124944579.png)





::: tip
Los educadores podrán revisar su afiliación ingresando a [scouts de argentina](https://scouts.org.ar/miembros-afiliados) 

en caso de que no aparezcas, debes comunicarte con tu JG, DD, DZ según corresponda a la función que desempeñes. Porque existe la posibilidad que te hayan afiliado con el número de DNI incorrecto y debe solucionarse desde el sistema Cruz del Sur por la autoridad que te haya afiliado.

:::

![image-20230817125148219](./image-20230817125148219.png)





------------------------------------------------

# Ingreso al Sistema

Pondrás tu DNI y darás clic en “(INGRESAR) VALIDAR DOCUMENTO

![image-20230817125238759](./image-20230817125238759.png)



Deberás poner tu contraseña, y en “Organismos”, al desplegar la aparecerán las funciones en los diferentes organismos que desempeñes.
Ahí deberás seleccionar la función con la que quieras ingresar. Si tienes una sola función aparece automáticamente y cliqueas en “**INGRESAR A LA PLATAFORMA**”



![image-20230817125315931](./image-20230817125315931.png)



 





 
