import { defineUserConfig, defaultTheme } from "vuepress";

export default defineUserConfig({
  lang: "es-ES",
  title: "Mi Rincon",
  description: "Mi lugar scout",
  theme: defaultTheme({
    logo: "/images/logo.png",
    colorMode: "light",
    // lastUpdated: false,
    lastUpdatedText: "Última Actualizacion",
    contributors: false,
    backToHome: "Atras",

    repo: "openScouts/mirincon-documentacion",
    editLinkText: "Editar",
    docsBranch: "main",
    docsDir: "docs",

    navbar: [
      {
        text: "Mi Rincon",
        link: "https://mirincon.ar",
      },
    ],
    sidebar: [
      {
        text: "Ingreso al Sistema",
        link: "/ingreso/",
      },
      {
        text: "Educadores",
        link: "/main/",
        collapsible: true,
        children: [  "/main/inscripciones" , "/main/miformacion"],
      },
      {
        text: "Eventos",
        link: "/eventos/",
        collapsible: true,
        children: [
          "/eventos/creacion",
          "/eventos/administrar",
          "/eventos/lugares",
        ],
      },
      {
        text: "Formacion",
        link: "/formacion/",
        collapsible: true,
        children: [
          "/formacion/experiencias",
          "/formacion/cursantes",
          "/formacion/certificados",
        ],
      },
    ],
  }),
});
