# Panel Principal

![image-20230817142613606](./assets/image-20230817142613606.png)



| ACCESOS                                                      | DESCRIPCIÓN                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20230817142701795](./assets/image-20230817142701795.png) | Encontraras los temas que se utilizan para personalizar el diseño del portal. - Perfil: modificaras tus datos como la foto, el mail, teléfono, profesión, celular y aclarar si necesitas o no una dieta especial en las experiencias de formación que incluyan comidas. También se puede cambiar la contraseña. |
| ![image-20230817142725253](./assets/image-20230817142725253.png) | Te permite acceder a la gestión de tu formación, ver en el acceso rápido las experiencias que tienen abiertas las inscripciones y a tu formación. |
| ![image-20230817142741068](./assets/image-20230817142741068.png) | Te permite acceder a la gestión de tu formación, ver las experiencias que tienen abiertas las inscripciones y a tu formación personal. |



