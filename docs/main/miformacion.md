# Mi Formación

[[toc]]

### Insignia de Madera
Visualización de IMs

![image-20230817143334828](./assets/image-20230817143334828.png)






### Formación Cursada
La Herramienta permite visualizar todas las experiencias realizadas,

::: tip
 Es importante tener en cuenta, que los responsables de que aparezcan cargados en Mi Rincon las experiencias cursadas es del Asistente De Adultos.
:::

![image-20230817143403119](./assets/image-20230817143403119.png)

Desde el listado de Formación, se podrá descargar los certificados de las experiencias cursadas.

::: tip
Solo podrán descargas los certificados de las experiencias aprobadas y que el organismo organizador alla definido que Mi Rincon generaba el certificado
:::